# Benjamin Burton's Resume

## Usage

Install MacTex:
```
brew cask install mactex
```

Run build:
```
./build.sh
```

## Resources

[Download as PDF](dist/resume.pdf)
![Resume Preview](dist/resume.png)
